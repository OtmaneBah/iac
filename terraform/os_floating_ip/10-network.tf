resource "openstack_networking_floatingip_v2" "metalLB_ip" {
    pool = "external"
    description = "MetalLB IP"
}