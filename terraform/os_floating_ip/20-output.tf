resource "local_file" "ip" {
  content = openstack_networking_floatingip_v2.metalLB_ip.address
  filename = "../resources/floating_ip/metallb_ip"
}