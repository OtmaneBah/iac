variable "nb_workers" {
  type    = string
  default = 3
}

variable "nb_masters" {
  type    = string
  default = 1
}

variable "nb_bastion" {
  type    = string
  default = 1
}

###
# Sec var
###

variable "worker_allow_ports" {
  type = map(any)
  default = {
    "kubelet API" = {
      from_port   = "10250"
      ip_protocol = "TCP"
      to_port     = "10250"
    },
    "NodePort Service" = {
      from_port   = "30000"
      ip_protocol = "TCP"
      to_port     = "32767"
    },
  }
}

variable "master_allow_ports" {
  type = map(any)
  default = {
    "K8S API server" = {
      from_port   = "6443"
      ip_protocol = "TCP"
      to_port     = "6443"
    },
    "Etcd server client API" = {
      from_port   = "2379"
      ip_protocol = "TCP"
      to_port     = "2380"
    },
    "Kubelet API" = {
      from_port   = "10250"
      ip_protocol = "TCP"
      to_port     = "10250"
    },
    "kube-scheduler" = {
      from_port   = "10251"
      ip_protocol = "TCP"
      to_port     = "10251"
    },
    "kube-controller-manager" = {
      from_port   = "10252"
      ip_protocol = "TCP"
      to_port     = "10252"
    },
  }
}

variable "icmp_ssh_allow_ports" {
  type = map(any)
  default = {
    "SSH" = {
      from_port   = "22"
      ip_protocol = "TCP"
      to_port     = "22"
    },
    "ICMP" = {
      from_port   = "-1"
      ip_protocol = "ICMP"
      to_port     = "-1"
    }
  }
}

variable "http_https_allow_ports" {
  type = map(any)
  default = {
    "http" = {
      from_port   = "80"
      ip_protocol = "TCP"
      to_port     = "80"
    },
    "https" = {
      from_port   = "443"
      ip_protocol = "TCP"
      to_port     = "443"
    },
  }
}

variable "weave_net_allow_ports" {
  type=map(any)
  default = {
    "6783-TCP" = {
      from_port   = "6783"
      ip_protocol = "TCP"
      to_port     = "6783"
    },
    "6783-UDP"= {
      from_port   = "6783"
      ip_protocol = "UDP"
      to_port = "6783"
    },
    "6784-UDP" = {
      from_port   = "6784"
      ip_protocol = "UDP"
      to_port = "6784"
    }
  }
}

variable "metallb-allow-ports" {
  type = map(any)
  default = {
    "http" = {
      from_port   = "7946"
      ip_protocol = "TCP"
      to_port     = "7946"
    },
    "https" = {
      from_port   = "7946"
      ip_protocol = "UDP"
      to_port     = "7946"
    },
  }
}



###
# SSH vars
###

variable "ssh_key_name" {
  type    = string
  default = "openstack_vdi"
}

##
# Network vars
##


variable "external_network_name" {
  type    = string
  default = "external"
}

variable "external_router_name" {
  type    = string
  default = "K8S_external_router"
}

variable "internal_subnet_name" {
  type    = string
  default = "K8s_internal_sub"
}

variable "internal_net_name" {
  type    = string
  default = "K8s_internal_net"
}

variable "subnet_ip_range" {
  type    = string
  default = "172.16.0.0/24"
}

variable "dns" {
  type    = list(string)
  default = ["192.44.75.10", "192.108.115.2"]
}
