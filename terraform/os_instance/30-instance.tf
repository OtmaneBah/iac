# ===
# Instances
# ===

resource "openstack_compute_instance_v2" "std_srv" {
  count           = var.nb_instance
  name            = "${var.instance_name}-${count.index}"
  image_name      = var.image_name
  flavor_name     = var.flavor_name
  key_pair        = data.openstack_compute_keypair_v2.user_key.name
  security_groups = var.security_groups
  network {
    name = data.openstack_networking_network_v2.internal_net.name
  }
}

# Attach floating ip to instance
resource "openstack_compute_floatingip_associate_v2" "http" {
  count       = var.enable_fip ? 1 : 0
  floating_ip = openstack_networking_floatingip_v2.fip_http[0].address
  instance_id = openstack_compute_instance_v2.std_srv[0].id
}
