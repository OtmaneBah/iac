# ===
# Network
# ===

data "openstack_networking_network_v2" "ext_network" {
  name = var.external_network_name
}

resource "openstack_networking_router_v2" "ext_router" {
  name = var.external_router_name
  admin_state_up      = true
  external_network_id = data.openstack_networking_network_v2.ext_network.id
}

resource "openstack_networking_network_v2" "internal_net" {
  name           = var.internal_net_name
  admin_state_up = true
}

resource "openstack_networking_subnet_v2" "internal_sub" {
  name            = var.internal_subnet_name
  network_id      = openstack_networking_network_v2.internal_net.id
  cidr            = var.subnet_ip_range
  dns_nameservers = var.dns
}

# Router interface configuration
resource "openstack_networking_router_interface_v2" "router_interface_1" {
  count     = var.enable_ext_net ? 1 : 0
  router_id = openstack_networking_router_v2.ext_router.id
  subnet_id = openstack_networking_subnet_v2.internal_sub.id
}


